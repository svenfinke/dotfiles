My public dotfiles. This will grow over time and make it easier for me to setup a new system while keeping a basic set of configurations.

# Usage

Feel free to clone or fork this repository if you like these settings or want to tweak them a little bit.

# Branching

I do not use any kind of branching as of now, but this might be used in the future, to make changes for specific environments.